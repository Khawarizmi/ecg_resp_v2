import os
import pandas as pd
import numpy as np


def get_dist(s):
    # to find the idx at cumsum = 1, i.e. first peak
    min_idx = s[s == max(min(s), 1)].index[0]

    # to find the idx at last peak
    max_idx = s[s == max(s)].index[0]
    return max_idx - min_idx


def get_r(s, fs=50):
    cumsum = s.cumsum()
    dist = get_dist(cumsum)
    return (s.sum()-1)/(dist/fs)*60


# calculate hr & rr at beats per min, bpm, given a df and sampling frequency, fs
def get_rate(d, fs=50):
    d = d.copy()
    hr = get_r(d['ecg_idx'])
    rr = get_r(d['resp_idx'])
    return (hr, rr)

# read


def read_df(filenames, processed_dir, i=0, j=0):
    # formatting data
    fullfilename = 'subsampled_'+filenames[i]+'_'+str(j)+'.csv'
    df_csv = pd.DataFrame()

    # if windows
    if os.name == 'nt':
        df_csv = pd.read_csv(processed_dir+fullfilename)
    else:
        df_csv = pd.read_csv(processed_dir+fullfilename)

    # filter data both rows & columns
    df = df_csv.copy()
    # df = df[['hrs', 'resp']]

    # add time from index
    df['time'] = df.index

    # remove outliers
    min_hrs = df['hrs'].mean()-3*df['hrs'].std()
    max_hrs = df['hrs'].mean()+3*df['hrs'].std()

    df = df[(df['hrs'] > min_hrs) & (df['hrs'] < max_hrs)]

    min_resp = df['resp'].mean()-3*df['resp'].std()
    max_resp = df['resp'].mean()+3*df['resp'].std()

    df = df[(df['resp'] > min_resp) & (df['resp'] < max_resp)]

    # # add unique id everytime hrs change
    # df['id'] = (df[['hrs']].diff() != 0).cumsum()

    # add unique id every 500 samples, i.e. 10 second, ~ 25 beats || ~10 resp cycle
    df['id'] = round(df['time']/500)

    return df


# calculate heart and respiratory rate
def gen_dfr(df, mult=500):
    hr_list = []
    rr_list = []
    id_list = []
    info = []

    r = round(df.shape[0]/mult)

    for i in range(r):
        try:
            start = i*mult
            end = (i+1)*mult
            hr, rr = get_rate(df.loc[start:end])
            hr_list.append(hr)
            rr_list.append(rr)
            # info.append([i, start, end])
            id_list.append(i)
        except Exception as e:
            print(e)

    # dfr = pd.DataFrame({'hr': hr_list, 'rr': rr_list, 'info': info})
    return pd.DataFrame({'rr': rr_list, 'hr': hr_list,  'id': id_list})

# grouping data


def gen_dfg(df, max_length=501):
    dfr = gen_dfr(df)
    # group data into unique continuous hrs, i.e. the generated id
    dfg = df.groupby('id')['resp'].apply(list).reset_index(name='resp')

    # add back corresponding hrs to each id
    dfg = pd.merge(dfg, dfr[['id', 'rr', 'hr']], on='id', how='left')

    # pad data to match maxed length
    dfg['resp'] = dfg['resp'].apply(
        lambda x: np.pad(x, (0, max_length-len(x)), 'mean'))

    # display data
    dfg = dfg.dropna()
    return dfg


# reformatting data
def gen_numpy(dfg, x_col='resp', y_col='hr'):
    X = np.array(dfg[x_col].tolist())
    r = np.array(range(X.shape[0]))
    X = X.astype(np.float)
    y = dfg[y_col].to_numpy()
    return (X, y)


def gen_flat_df(df, c='resp'):
    dff = df.copy()
    val = dff[c].apply(len).max()
    cols = [c+str(i) if i != 0 else c for i in range(val)]
    dff[cols] = dff[c].apply(pd.Series).fillna('')

    return dff

# def gen_dft(d):
#     # filter data both rows & columns
#     dfg = d.copy()
